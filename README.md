# itpm-bios

TPM bios update tools distributed as python package itpm_bios_v1_6

## Requirements

`itpm_bios_v1_6` needs [pyFABIL](https://gitlab.com/ska-telescope/pyfabil) installed (v1.2 or later). If not already available in your environment please install as follow:
```
$ pip install git+https://gitlab.com/ska-telescope/pyfabil.git
```
If you experiments trouble installing pyFABIL, please try updating pip:
```
$ python -m pip install --upgrade pip
```

## Installation

To install the package run following command:
```
$ pip install git+https://gitlab.com/sanitaseg/itpm-bios.git
```

## Usage

`itpm_bios_v1_6` can be used to update a TPM v1.6 board, you needs to specify board ip address and bios version.

```
$ python -m itpm_bios_v1_6 --ip 10.0.10.2 --bios v0.3.0
==============================================================
PLEASE READ THE AGREEMENT CAREFULLY.
BY USING THIS SOFTWARE, YOU ACCEPT THE TERMS OF THE AGREEMENT.
You can read license by '--show-license' option
==============================================================

| BOARD INFO     |                                                        |
|:---------------|:-------------------------------------------------------|
| ip_address     | 10.0.10.2                                              |
| netmask        | 255.255.255.0                                          |
| gateway        | 255.255.255.255                                        |
| ip_address_eep | 10.0.10.2                                              |
| netmask_eep    | 255.255.255.0                                          |
| gateway_eep    | 255.255.255.255                                        |
| MAC            | 04:91:62:b2:38:e7                                      |
| SN             | 0850423010002                                          |
| PN             | iTPM_ADU_2.0                                           |
| bios           | v0.2.2 (CPLD_0x22032409-MCU_0xb0000118_0x20220831_0x0) |
| BOARD_MODE     | NO-ADA                                                 |
| LOCATION       | 65535:255:255                                          |
| HARDWARE_REV   | v2.0.0                                                 |
| DDR_SIZE_GB    | 4                                                      |

| BIOS      |                                                        |
|:----------|:-------------------------------------------------------|
| ACTUAL    | v0.2.2 (CPLD_0x22032409-MCU_0xb0000118_0x20220831_0x0) |
| REQUESTED | v0.3.0 (CPLD_0x23011618-MCU_0xb000011a_0x20230126_0x0) |

=============== WARNING !!! ===================
TPM BIOS Update procedure ready to start.
This procedure will delete the board BIOS.
You execute this procedure at your risk.
Do NOT power off the board, while process start.
Do you want continue (y/N)
y
Using CPLD bitfile /home/user/venv/lib/python3.8/site-packages/itpm_bios_v1_6/v0.3.0/itpm_v16_impl1_XO3L_23011618.bit
Open Bistream file /home/user/venv/lib/python3.8/site-packages/itpm_bios_v1_6/v0.3.0/itpm_v16_impl1_XO3L_23011618.bit
Loading /home/user/venv/lib/python3.8/site-packages/itpm_bios_v1_6/v0.3.0/itpm_v16_impl1_XO3L_23011618.bit (157678 bytes) = 3 * 65536 bytes sectors
Sector 007 @ 00070000: Erasing
Sector 006 @ 00060000: Erasing
Sector 005 @ 00050000: Erasing
Sector 004 @ 00040000: Erasing
Sector 003 @ 00030000: Erasing
Sector 002 @ 00020000: Erasing
Sector 001 @ 00010000: Erasing
Bitstream write complete
elapsed time 8.304104089736938s
Using MCU bitfile:
/home/user/venv/lib/python3.8/site-packages/itpm_bios_v1_6/v0.3.0/iTPM_ADU_v1_5_MCU_FW_20230126.bin
FW size: 70016 bytes
Start MCU Monitor
Waiting few seconds for MCU boot ...
Erasing flash...
Programming flash...
 |##################################################| 100.0%
Elapsed time 175.480302
Please power off and power on the board, then press Enter

Waiting 3 seconds for board boot ...

| BIOS     |                                                        |
|:---------|:-------------------------------------------------------|
| PREVIOUS | v0.2.2 (CPLD_0x22032409-MCU_0xb0000118_0x20220831_0x0) |
| ACTUAL   | v0.3.0 (CPLD_0x23011618-MCU_0xb000011a_0x20230126_0x0) |
| EXPECTED | v0.3.0 (CPLD_0x23011618-MCU_0xb000011a_0x20230126_0x0) |

You can read bios CHANGELOG by '--show-changelog' option
Operation successful!!
```

If you don't provide a bios version it show board details and available bios revision.

```
$ python -m itpm_bios_v1_6 --ip 10.0.10.2
==============================================================
PLEASE READ THE AGREEMENT CAREFULLY.
BY USING THIS SOFTWARE, YOU ACCEPT THE TERMS OF THE AGREEMENT.
You can read license by '--show-license' option
==============================================================

| BOARD INFO     |                                                        |
|:---------------|:-------------------------------------------------------|
| ip_address     | 10.0.10.2                                              |
| netmask        | 255.255.255.0                                          |
| gateway        | 255.255.255.255                                        |
| ip_address_eep | 10.0.10.2                                              |
| netmask_eep    | 255.255.255.0                                          |
| gateway_eep    | 255.255.255.255                                        |
| MAC            | 04:91:62:b2:38:e7                                      |
| SN             | 0850423010002                                          |
| PN             | iTPM_ADU_2.0                                           |
| bios           | v0.2.2 (CPLD_0x22032409-MCU_0xb0000118_0x20220831_0x0) |
| BOARD_MODE     | NO-ADA                                                 |
| LOCATION       | 65535:255:255                                          |
| HARDWARE_REV   | v2.0.0                                                 |
| DDR_SIZE_GB    | 4                                                      |

ERROR - No bios provided or bios not available
Please specify one of bios below compatible with HW_rev 0x20000
 - v0.2.1 (0x21061417-0xb0000117_0x20210615_0x0)
 - v0.2.2 (0x22032409-0xb0000118_0x20220831_0x0)
 - v0.3.0 (0x23011618-0xb000011a_0x20230126_0x0)
```

## Command help

```
$ python -m itpm_bios_v1_6 --help
Usage: __main__.py [options]

Options:
  -h, --help            show this help message and exit
  --ip=IP               Board IP [default: 10.0.10.2]
  --port=PORT           Port [default: 10000]
  --bios=BIOS_VERSION   Bios version to be loaded (e.g. v0.2.0 or 0.2.0)
  --force               skip confimations and final check
  --show-changelog      Show bios CHANGELOG
  --show-license        Show bios LICENSE
  --show-bios           Show bios list
```
