BIOS_REV_list = [
    {
        'name': 'itpm_v1.6',
        'HW_REV_ge': 0x010600,
        'HW_REV_l': None,
        'bios': [
            {
                'rev': '0.2.1',
                'cpld': {
                    'id': '0x21061417',
                    'file': 'v0.2.1/itpm_v16_impl1_XO3L_14062021.bit'},
                'mcu': {
                    'id': '0xb0000117_0x20210615_0x0',
                    'file': 'v0.2.1/iTPM_ADU_v1_5_MCU_FW-0xb0000117_sov.bin'},
            },
            {
                'rev': '0.2.2',
                'cpld': {
                    'id': '0x22032409',
                    'file': 'v0.2.2/itpm_v16_impl1_XO3L.bit'},
                'mcu': {
                    'id': '0xb0000118_0x20220831_0x0',
                    'file': 'v0.2.2/iTPM_ADU_v1_5_MCU_FW_310820222.bin'},
            },
            {
                'rev': '0.3.0',
                'cpld': {
                    'id': '0x23011618',
                    'file': 'v0.3.0/itpm_v16_impl1_XO3L_23011618.bit'},
                'mcu': {
                    'id': '0xb000011a_0x20230126_0x0',
                    'file': 'v0.3.0/iTPM_ADU_v1_5_MCU_FW_20230126.bin'},
            },
            {
                'rev': '0.3.1',
                'cpld': {
                    'id': '0x23011618',
                    'file': 'v0.3.0/itpm_v16_impl1_XO3L_23011618.bit'},
                'mcu': {
                    'id': '0xb000011a_0x20230209_0x0',
                    'file': 'v0.3.1/iTPM_ADU_v1_5_MCU_FW_20230209.bin'},
            },
            {
                'rev': '0.4.0',
                'cpld': {
                    'id': '0x23090714',
                    'file': 'v0.4.0/itpm_v16_impl1_XO3L_23090714.bit'},
                'mcu': {
                    'id': '0xb000011a_0x20230209_0x0',
                    'file': 'v0.3.1/iTPM_ADU_v1_5_MCU_FW_20230209.bin'},
            },
            {
                'rev': '0.5.0',
                'cpld': {
                    'id': '0x23092511',
                    'file': 'v0.5.0/itpm_v16_impl1_XO3L_23092511.bit'},
                'mcu': {
                    'id': '0xb000011a_0x20230209_0x0',
                    'file': 'v0.3.1/iTPM_ADU_v1_5_MCU_FW_20230209.bin'},
            },
        ],
    },
]

def get_bios(bios):
    string = "v%s (%s)" % (bios['rev'], bios_id_format(bios))
    return string

def bios_id_format(bios):
    string = "CPLD_"
    string += bios['cpld']['id']
    string += "-MCU_"
    string += bios['mcu']['id']
    return string

def bios_get_dict(name):
    for _elem in BIOS_REV_list:
        if name == _elem['name']:
            _result = []
            for _bios in _elem['bios']:
                _res_bios = [_bios['rev'], bios_id_format(_bios)]
                _result.append(_res_bios)
            return _result
    return None
