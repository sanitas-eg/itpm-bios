__author__ = 'Cristian Albanese'
import os
import sys
import time
import tabulate
from pkg_resources import resource_filename

import itpm_bios_v1_6
from pyfabil.boards.tpm_1_6 import TPM_1_6


def check_update_files(path, bios):
    dirs = os.listdir(path)
    if dirs is not None:
        # print(dirs)
        dirs_ordered = sorted(dirs)
        # print(dirs_ordered)
        # folder = dirs_ordered[len(dirs_ordered)-1]
        folder = ""
        for i in range(0, len(dirs_ordered)):
            if dirs_ordered[i].find(bios) != -1:
                folder = dirs_ordered[i]
        if folder == "":
            print("Error: request bios folder not found")
            return "", ""
        # print folder
        fws = os.listdir(path+folder+"/")
        if len(fws) < 2:
            print("Error, incorrect number of file in path")
        else:
            # print(fws)
            fw_mcu = ""
            fw_cpld = ""
            for i in range(0, len(fws)):
                if fws[i].find("MCU") != -1 and fws[i].find(".bin") != -1:
                    fw_mcu = fws[i]
                if fws[i].find("XO3L") != -1 and fws[i].find(".bit") != -1:
                    fw_cpld = fws[i]
            if (fw_mcu!="" and fw_cpld!=""):
                print("FW MCU %s" % fw_mcu)
                print("FW CPLD %s" % fw_cpld)
                return fw_mcu, fw_cpld
            else:
                print("Error: unexpected file")
                return "", ""

def file_exist(file_path):
    if not os.path.exists(file_path):
        print("Error: cannot find file:")
        print(file_path)
        sys.exit(1)

def program_cpld(tpm,cpld_fw):
    print("Using CPLD bitfile {}".format(cpld_fw))
    #tile.program_cpld(conf.bitfile)
    starttime = time.time()
    cpld_FW_start_add=0x10000
    ec=tpm.tpm_progflash.firmwareProgram(0,cpld_fw,cpld_FW_start_add,erase_size=0x70000)
    endtime = time.time()
    delta = endtime - starttime
    if ec==0:
        print("Bitstream write complete")
        print("elapsed time " + str(delta) + "s")
        return True
    else:
        print("Error detected while bitsream writing in flash")
        return False

if __name__ == "__main__":
    # Use OptionParse to get command-line arguments
    import optparse
    from sys import argv, stdout

    parser = optparse.OptionParser()
    parser.add_option("--ip", action="store", dest="ip",
                    default="10.0.10.2", help="Board IP [default: 10.0.10.2]")
    parser.add_option("--port", action="store", dest="port",
                    type="int", default="10000", help="Port [default: 10000]")
    parser.add_option("--bios", action="store", dest="bios_version",
                    default=None, help="Bios version to be loaded (e.g. v0.2.0 or 0.2.0)")
    parser.add_option("--force", action="store_true", dest="force",
                    default=False, help="skip confimations and final check")
    parser.add_option("--show-changelog", action="store_true", dest="show_changelog",
                    default=False, help="Show bios CHANGELOG")
    parser.add_option("--show-license", action="store_true", dest="show_license",
                    default=False, help="Show bios LICENSE")
    parser.add_option("--show-bios", action="store_true", dest="show_bios",
                    default=False, help="Show bios list")
    parser.add_option("-P", "--path", action="store", dest="file_path",
                    default=None, help=optparse.SUPPRESS_HELP)

    (conf, args) = parser.parse_args(argv[1:])
    if conf.show_changelog:
        print("=== CHANGELOG.md ===")
        f=open(resource_filename('itpm_bios_v1_6','CHANGELOG.md'),'r')
        print(f.read())
        sys.exit(0)
    if conf.show_license:
        print("=== LICENSE ===")
        f=open(resource_filename('itpm_bios_v1_6','LICENSE'),'r')
        print(f.read())
        sys.exit(0)
    if conf.show_bios:
        config=itpm_bios_v1_6.bios.BIOS_REV_list
        print("Available bios:")
        for _board in config:
            print(" + "+_board['name'])
            for _bios in _board['bios']:
                print("     - v%s (%s-%s)"%(_bios['rev'],_bios['cpld']['id'],_bios['mcu']['id']))
            sys.exit(1)
    print("==============================================================")
    print("PLEASE READ THE AGREEMENT CAREFULLY.")
    print("BY USING THIS SOFTWARE, YOU ACCEPT THE TERMS OF THE AGREEMENT.")
    print("You can read license by '--show-license' option")
    print("==============================================================")
    print("")
    try:
        tpm = TPM_1_6(ip=conf.ip)
        tpm.connect(ip=conf.ip, port = conf.port)
        board_info=tpm.get_board_info()
        table=[]
        for key in board_info:
            table.append([str(key), str(board_info[key])])
        print(tabulate.tabulate(table,headers=["BOARD INFO",""],tablefmt='pipe'))
        print("")
        hardware_rev=tpm.get_hardware_revision()
    except:
        print("ERROR - No TPM found at ip address %s"%conf.ip)
        sys.exit(1)
    cpld_update=True
    mcu_update=True



    if conf.file_path is None:
        config=itpm_bios_v1_6.bios.BIOS_REV_list

        for _elem in config:
            ge_match=False
            le_match=False
            if _elem['HW_REV_ge'] is not None:
                if hardware_rev >= _elem['HW_REV_ge']:
                    ge_match=True
            else:
                ge_match=True
            if _elem['HW_REV_l'] is not None:
                if hardware_rev < _elem['HW_REV_l']:
                    le_match=True
            else:
                le_match=True
            if ge_match and le_match:
                break
        if not ge_match or not le_match:
            print("ERROR - No bios match for HW_rev 0x%x"%hardware_rev)
            sys.exit(1)

        bios_match = False
        if conf.bios_version is not None:
            for _bios in _elem['bios']:
                if conf.bios_version.lstrip('v') == _bios['rev']:
                    bios_match=True
                    break
        if conf.bios_version is None or not bios_match:
            print("ERROR - No bios provided or bios not available")
            print("Please specify one of bios below compatible with HW_rev 0x%x"%hardware_rev)
            for _bios in _elem['bios']:
                print(" - v%s (%s-%s)"%(_bios['rev'],_bios['cpld']['id'],_bios['mcu']['id']))
            sys.exit(1)

        if os.path.isabs(_bios['cpld']['file']):
            print("ERROR - CONFIG FILE path must be relative")
            sys.exit(1)
        else:
            cpld_fw=resource_filename('itpm_bios_v1_6',_bios['cpld']['file'])
        file_exist(cpld_fw)
        if tpm.tpm_cpld.get_version() == _bios['cpld']['id']:
            cpld_update=False
            print("INFO - CPLD already updated to requested version")

        if os.path.isabs(_bios['mcu']['file']):
            print("ERROR - CONFIG FILE path must be relative")
            sys.exit(1)
        else:
            mcu_fw=resource_filename('itpm_bios_v1_6',_bios['mcu']['file'])
        file_exist(mcu_fw)
        if tpm.tpm_monitor.get_version() == _bios['mcu']['id']:
            mcu_update=False
            print("INFO - MCU already updated to requested version")

        req_bios = itpm_bios_v1_6.bios.get_bios(_bios)
        act_bios = tpm.get_bios()

    else:
        print("request path: %s" % conf.file_path)
        req_bios = None
        act_bios = tpm.get_bios()
        print("Board actual Bios: %s" % act_bios)
        mcu_fw, cpld_fw = check_update_files(conf.file_path, conf.bios_version)

        if mcu_fw == "" or cpld_fw == "":
            print("ERROR - mcu_fw or cpld_fw not available")
            sys.exit(1)

        cpld_fw = conf.file_path + conf.bios_version + "/" + cpld_fw
        mcu_fw =  conf.file_path + conf.bios_version + "/" + mcu_fw

    if (not cpld_update and not mcu_update):
        print("INFO - Board already updated to the required version")
        sys.exit(0)

    table=[]
    table.append(["ACTUAL",str(act_bios)])
    table.append(["REQUESTED",str(req_bios)])
    print(tabulate.tabulate(table,headers=["BIOS",""],tablefmt='pipe'))
    print("")

    print("=============== WARNING !!! ===================")
    print("TPM BIOS Update procedure ready to start.")
    print("This procedure will delete the board BIOS.")
    print("You execute this procedure at your risk.")
    print("Do NOT power off the board, while process start.")
    if not conf.force:
        answ = input("Do you want continue (y/N)\n")
        if answ != "y" and answ != "Y":
            sys.exit(1)

    if cpld_update:
        if not program_cpld(tpm,cpld_fw):
            print("Error while CPLD FW UPDATE, please verify and retry without poweroff board")
            sys.exit(1)
    if mcu_update:
        mcu = itpm_bios_v1_6.mcu_update.mcu(tpm)
        if not mcu.update(mcu_fw):
            print("Error while CPLD FW UPDATE, please verify and retry without poweroff board")
            sys.exit(1)
    if not conf.force:
        print("Please power off and power on the board, then press Enter")
        answ = input("")
        print("Waiting 3 seconds for board boot ...")
        time.sleep(3)
        try:
            new_bios = tpm.get_bios()
        except:
            print('EXCEPTION: Connection error occurred!!!!, Possible Error While Update, Please retry')
            sys.exit(1)

        table=[]
        table.append(["PREVIOUS",str(act_bios)])
        table.append(["ACTUAL",str(new_bios)])
        table.append(["EXPECTED",str(req_bios)])
        print("")
        print(tabulate.tabulate(table,headers=["BIOS",""],tablefmt='pipe'))
        print("")
        print("You can read bios CHANGELOG by '--show-changelog' option")
        if req_bios is not None:
            if req_bios == new_bios:
                print("Operation successful!!")
                sys.exit(0)
            else:
                print("Operation failed!!")
                sys.exit(1)
        else:
            print("Operation successful!!")
            sys.exit(0)
    else:
        print("Warning! Final check skipped by --force options")
        print("Please power off and power on the board, and verify bios manually")
        sys.exit(0)
