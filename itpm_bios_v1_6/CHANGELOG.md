# Changelog

## v0.5.0 (2023-09-25)
bios_id: `CPLD_0x23092511-MCU_0xb000011a_0x20230209_0x0`
### Bug Fixes
- Better power-on reset management for Ethernet PHY, MCU and CPLD (infrequent TPM unreachable).

## v0.4.0 (2023-09-07)
bios_id: `CPLD_0x23090714-MCU_0xb000011a_0x20230209_0x0`
### Features
- ethernet gateway function

## v0.3.1 (2023-02-09)
bios_id: `CPLD_0x23011618-MCU_0xb000011a_0x20230209_0x0`
### Features
- improve power-good IRQ management
### Bug Fixes
- over-temperature protection double check on error
- MCU watchdog expiration at 1s (was about 60s)

## v0.3.0 (2023-01-26)
bios_id: `CPLD_0x23011618-MCU_0xb000011a_0x20230126_0x0`
### Features
- alarm management
- MCU watchdog protection
- over-temperature protection (default 80°C board temperature)
- over-voltage protection (default disabled)
- peripheral lock mechanism (i2c, smap) for concurrent MCU/sw access
- improve bios stability

## v0.2.2 (2022-08-31)
bios_id: `CPLD_0x22032409-MCU_0xb0000118_0x20220831_0x0`
### Features
- 1 hour timebomb for boards without license
### Bug Fixes
- MCU deadlock at boot

## v0.2.1 (2021-07-20)
bios_rev: `CPLD_0x21061417-MCU_0xb0000117_0x20210615_0x0`
### Bug Fixes
- MCU and UDP wisbone access problem (fix problem detected on TPM delivered in 2021)

## v0.2.0 (2021-03-31)
bios_id: `CPLD_0x21033009-MCU_0xb0000107_0x20210114_0x0`
### Features
- UPDATE MCU management
- MCU monitor and safety function  
- **note** released for delivery-2021
### Bug Fixes
- SingleWire bus for subrack
