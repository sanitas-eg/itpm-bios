__author__ = 'Cristian Albanese'
"""This Script permit to read/write to CPLD2MCU serila interface"""
import sys
import time
from optparse import OptionParser
import os
from console_progressbar import ProgressBar

sys.path.append("../")
from pyfabil.boards.tpm_1_6 import TPM_1_6

import struct

usage_string = "usage: %prog [options] [<value>]\n"
usage_hexample = "es. %prog -s 0x0a\nes. %prog -r\n"


# for SAM4S
class flash_cmd():
    CLRGPNVM = "W400E0A04,5A00010C#"
    SETGPNVM = "W400E0A04,5A00010B#"
    ERASEWRITEPG= ""
    ERASEALL=""
    ERASESECT0 = "W400E0A04,5A000011#"
    ERASESECT1 = "W400E0A04,5A001011#"
    ERASESECTL = "W400E0A04,5A002011#"
    CLRLOCK=""
    SETLOCK=""
    WRITEPAGE = "W400E0A04,5A000103#"
    WRITEPAGEERASE_CMD = "W400E0A04"
    WRITEPAGE_ARG = 0x5A000001
    WRITEPAGEERASE_ARG = 0x5A000003


PAGE_SIZE = 512
SECTOR_PAGE = 128
MCU_NVMCTRL_BA = 0x41004000
BOOTLOADER_SIZE = 2048
FLASH_BASE_ADDRESS = 0x00000000
FLASH_APP_ADDRESS = FLASH_BASE_ADDRESS + BOOTLOADER_SIZE


# for SAMAD21
# class for SAMD21 Flash Ctrl managem
class NVMCTRL():
    CTRLA_OFFSET = 0x0000
    CTRLB_OFFSET = 0x0004
    PARAM_OFFSET = 0x0008
    STATUS_OFFSET = 0x0018
    INTFLAG_OFFSET = 0x0014
    ADDRESS_OFFSET = 0x001C
    CTRLB_MANW = (1 << 7)
    INTFLAG_READY = (1 << 0)
    INTFLAG_ERROR = (1 << 1)
    CTRLA_CMDA = {
        'ER': 0x02,
        'WP': 0x04,
        'PBC': 0x44,
    }
    PAGES_PER_ROW = 4
    PAGE_SIZE = 64
    PAGES = 4096

    def __init__(self, tpm):
        """Initializes a NVMCTRL controller instance at the specified base
           address in the attached device.

           Args:
               tpm : ITPM tpm instance
        """
        self.base_address = MCU_NVMCTRL_BA
        self.samba = samba_op(tpm)
        self.page_size = self.PAGE_SIZE
        self.pages = self.PAGES

    def _chunk(self, flash_page_size, address, data):
        """Helper method for subclasses; chunks the given data into flash pages,
           aligned to single flash pages within the target's address space.

           Args:
              flash_page_size : Size of each flash page in the target device.
              address         : Start address of the data to write to.
              data            : Data to be written to the target device.

           Returns:
              Generator of (address, chunk) tuples for each chunk of data to
              write.
        """

        chunk = []

        for offset in range(len(data)):
            if offset and (address + offset) % flash_page_size == 0:
                yield(address, chunk)

                address += flash_page_size
                chunk = []

            chunk.append(data[offset])

        if len(chunk):
            yield (address, chunk)

    def _command(self, command):
        """Issues a low-level command to the NVMCTRL module within the
            connected device.

           Args:
              command : Command value to issue (see `CTRLA_CMDA`)
        """
        reg = (0xA5 << 8) | command
        self.samba.write_half_word(self.base_address + self.CTRLA_OFFSET, reg)

    def erase_flash(self, start_address, end_address=None):
        """Erases the device's application area in the specified region.

           Args:
              start_address : Start address to erase.
              end_address   : End address to erase (or end of application area
                              if `None`).
        """

        if end_address is None:
            end_address = self.pages * self.page_size

        start_address -= start_address % (self.PAGES_PER_ROW * self.page_size)
        end_address -= end_address % (self.PAGES_PER_ROW * self.page_size)
        # print ("Erase start address %x, end address %x" %(start_address,end_address))

        for offset in range(start_address, end_address, self.PAGES_PER_ROW * self.page_size):
            self.samba.write_word(self.base_address + self.ADDRESS_OFFSET, offset >> 1)

            self._command(self.CTRLA_CMDA['ER'])
            time.sleep(0.01)

    def program_flash(self, address, data):
        """Program's the device's application area.

           Args:
              address : Address to program from.
              data    : Data to program into the device.
        """
        self.samba.write_word(self.base_address + self.CTRLB_OFFSET, self.CTRLB_MANW)
        self._command(self.CTRLA_CMDA['PBC'])
        time.sleep(0.01)
        page = 0
        page_num = len(data)/self.page_size
        pb = ProgressBar(total=100,prefix='', suffix='', decimals=1, length=50, fill='#', zfill='-')
        for (chunk_address, chunk_data) in self._chunk(self.page_size, address, data):
            for offset in range(0, len(chunk_data), 4):
                word = sum([x << (8 * i) for i, x in enumerate(chunk_data[offset : offset + 4])])
                self.samba.write_word(chunk_address + offset, word)
                # input("Press key to next write")
            # print("programming page %d" %page)
            page=page+1
            pb.print_progress_bar(page*100/page_num)
            self._command(self.CTRLA_CMDA['WP'])
            time.sleep(0.01)


class uart_operation:
    send = 0
    receive = 1


class McuUart:
    """ Management Class for CPLD2MCU Uart control for MCU update """
    def __init__(self, tpm):
        self.tpm = tpm
    # ######################################################################################

    def uart_send_byte(self, dataw):
        self.tpm["board.uart.txdata"] = ord(dataw)
        self.tpm["board.uart.rnw"] = uart_operation.send
        op_status=0
        start = time.time()
        while True:
            if self.tpm["board.uart.status"] & 0x1 == 0:
                break
            else:
                now = time.time()
                # print("[uart2mcu_write] time now %d" %now)
                if now-start > 5:
                    op_status = 1
                    break
        return op_status

    def uart_receive_byte(self):
        op_status = 0
        start = time.time()
        rxdata = 0
        while True:
            if self.tpm["board.uart.status"] & 0x2 == 0x2:
                self.tpm["board.uart.rnw"] = uart_operation.receive
                rxdata = self.tpm["board.uart.rxdata"]
                print("uart2mcu_read")
                break
            else:
                now= time.time()
                if now-start > 30:
                    print("[uart2mcu_read] time now %d" % now)
                    op_status = 1
                    break
        return rxdata, op_status

    def uart_send_buffer(self, databuff):
        op_status = 0
        rxdata = []
        start = time.time()
        # print("[uart2mcu_write] time start %.6f" % start)
        for i in range(0, len(databuff)):
            self.tpm["board.uart.txdata"]=databuff[i]
            self.tpm["board.uart.rnw"]=uart_operation.send
            # now = time.time()
            # print("[uart2mcu_write] time now %.6f" %now)
            while True:
                if self.tpm["board.uart.status"] & 0x1 == 0:
                    # now_while = time.time()
                    # print("[uart2mcu_write] time now_while %.6f" %now_while)
                    break
                else:
                    now = time.time()
                    # print("[uart2mcu_write] time now %d" %now)
                    if now - start > 1:
                        op_status = 1
                        return op_status
        return op_status

    def uart_send_buffer_wrx(self, databuff):
        op_status = 0
        rxdata = []
        start = time.time()
        # print("[uart2mcu_write] time start %.6f" % start)
        for i in range(0, len(databuff)):
            # self.tpm["board.uart.ctrl.txdata"]=databuff[i]
            # self.tpm["board.uart.ctrl.rnw"]=uart_operation.send
            self.tpm[0xb0000004]=databuff[i]
            self.tpm[0xb0000000]=uart_operation.send
            now = time.time()
            # print("[uart2mcu_write] time now %.6f" %now)
            while True:
                # if self.tpm["board.uart.ctrl.status"]&0x1==0:
                if self.tpm[0xb000000C] & 0x1 == 0:
                    now_while = time.time()
                    # print("[uart2mcu_write] time now_while %.6f" %now_while)
                    break
                else:
                    now = time.time()
                    # print("[uart2mcu_write] time now %d" %now)
                    if now - start > 1:
                        op_status = 1
                        return op_status
        while True:
            # if (self.tpm["board.uart.ctrl.status"]&0x2)==0x2:
            #    self.tpm["board.uart.ctrl.rnw"]=uart_operation.receive
            #    rxdata.append(self.tpm["board.uart.ctrl.rxdata"])
            if (self.tpm[0xb000000c] & 0x2) == 0x2:
                self.tpm[0xb0000000] = uart_operation.receive
                rxdata.append(self.tpm[0xb0000008])
                # print ("uart2mcu_read")
            else:
                now = time.time()
                if now-start > 1:
                    # print("[uart2mcu_read] time now %d" % now)
                    break
        return op_status, rxdata

    def start_mcu_sam_ba_monitor(self):
        print("Start MCU Monitor")
        op_status = 0
        self.tpm["board.regfile.xo3_link"] = 0  # set pin to bootloader start selection
        time.sleep(0.5)
        self.tpm["board.regfile.sam_nrst"] = 0  # assert reset MCU
        time.sleep(0.5)
        self.tpm["board.regfile.sam_nrst"] = 1  # deassert reset MCU
        time.sleep(0.5)
        self.tpm["board.regfile.xo3_link"] = 1 # restore pin to bootloader start selection
        time.sleep(0.5)
        return op_status

    def reset_mcu(self):
        self.tpm["board.regfile.sam_nrst"] = 0
        time.sleep(0.01)
        self.tpm["board.regfile.sam_nrst"] = 1
        time.sleep(0.1)


def loadBitstream(filename, pagesize):
    # print ("Open Bistream file %s" % (filename))
    with open(filename, "rb") as f:
        dump = bytearray(f.read())
    bitstreamSize = len(dump)

    pages = bitstreamSize // pagesize
    if (pages * pagesize) != bitstreamSize:
        pages = pages + 1
    # print("Loading %s (%d bytes) = %d * %d bytes pages" % (filename, bitstreamSize, pages, pagesize))
    s = int(pages * pagesize)
    tmp = bytearray(s)
    for i in range(0, bitstreamSize):
        tmp[i] = dump[i]
    for i in range(0, s - bitstreamSize):
        tmp[i + bitstreamSize] = 0xff
    return tmp, bitstreamSize, s


class samba_op:
    def __init__(self, tpm):
        self.tpm = tpm
        self.uart = McuUart(self.tpm)

    def write_word(self, address, data):
        dataw = []
        cmd = "W"+hex(address)[2:]+","+hex(data)[2:]+"#"
        # print("Data write: %s" %cmd)
        for i in range(0, len(cmd)):
            dataw.append(ord(cmd[i]))
        state = self.uart.uart_send_buffer(dataw)
        if state != 0:
            raise Exception("ERROR: TIMEOUT Occurred during write")

    def write_half_word(self, address, data):
        dataw = []
        cmd = "H"+hex(address)[2:]+","+hex(data)[2:]+"#"
        for i in range(0, len(cmd)):
            dataw.append(ord(cmd[i]))
        state = self.uart.uart_send_buffer(dataw)
        if state != 0:
            raise Exception("ERROR: TIMEOUT Occurred during write")


usage_string = "usage: %prog [options]\n"
usage_hexample = "es.(update FW) %prog -u -f <fw_binary_file> --ip <board ip>"


class mcu:
    def __init__(self, tpm):
        self.tpm = tpm
        self.uart = McuUart(self.tpm)

    def reset(self):
        """Reset MCU """
        print("resetting MCU...")
        self.uart.reset_mcu()

    def send(self, send):
        """send a character or string to MCU"""
        rxdata = []
        datar = ""
        dataw = []
        for i in range(0, len(send)):
            dataw.append(ord(send[i]))
        state, data = self.uart.uart_send_buffer_wrx(dataw)
        if state != 0:
            raise Exception("ERROR: TIMEOUT Occurred during write")
        # else:
        #     if data!=None:
        #         for i in range(0, len(data)):
        #             rxdata.append(data[i])
        # print (rxdata)
        # if rxdata != None:
        #     for i in range(0, len(rxdata)):
        #         datar = datar + chr(rxdata[i])
        #     print("Read data %s: " %datar)

    def erase(self, bitfile=None):
        """send Flash ERASE for first 2 FLASH sectors """
        print("Erase flash selected")
        nvm = NVMCTRL(self.tpm)
        if bitfile is not None:
            if os.path.exists(bitfile) and os.path.isfile(bitfile):
                # print("Using MCU bitfile {}".format(bitfile))
                memblock, bitstreamSize, size = loadBitstream(bitfile, nvm.PAGE_SIZE)
                # Read bitfile and cast as a list of unsigned integers
                formatted_bitstream = list(struct.unpack_from('I' * (len(memblock) // 4), memblock))
                cmd = ""
                page = 0
                start = time.time()
                pre = start
                sect = size//(64*1024)+1
                print("Erase size: %d bytes" % size)
                nvm.erase_flash(0x2000,0x2000+size)
            else:
                print("ERROR - Could not load bitfile {}, check filepath".format(bitfile))
                return False
        else:
            print("ERROR - No CPLD bitfile specified")
            return False
        return True

    def update(self, bitfile=None):
        """Update MCU"""
        nvm = NVMCTRL(self.tpm)
        _samba_op = samba_op(self.tpm)
        if bitfile is not None:
            if os.path.exists(bitfile) and os.path.isfile(bitfile):
                print("Using MCU bitfile:\n{}".format(bitfile))
                memblock, bitstreamSize, size = loadBitstream(bitfile, nvm.PAGE_SIZE)
                # Read bitfile and cast as a list of unsigned integers
                formatted_bitstream = list(struct.unpack_from('I' * (len(memblock) // 4), memblock))
                start = time.time()
                pre = start
                sect = size//(64*1024)+1
                print("FW size: %d bytes" % size)
                # start bootloader and samba monitor
                _samba_op.uart.start_mcu_sam_ba_monitor()
                print("Waiting few seconds for MCU boot ...")
                time.sleep(5)
                # rxdata = []
                # datar = ""
                # dataw = []
                # data=[ord("#")]
                # for i in range(0, len(data)):
                #     dataw.append(ord(data[i]))
                state, data = self.uart.uart_send_buffer_wrx([ord("#")])
                if state != 0:
                    print("ERROR: TIMEOUT Occurred during write")
                    return False
                # else:
                #     if data != None:
                #         for i in range(0, len(data)):
                #             rxdata.append(data[i])
                # # print (rxdata)
                # if rxdata != None:
                #     for i in range(0, len(rxdata)):
                #         datar = datar + chr(rxdata[i])
                #     print("Read data %s: " % datar)
                #     if (state != 0):
                #         print("ERROR: TIMEOUT Occurred during write")
                #         return False
                time.sleep(0.1)
                # Erase flash
                print("Erasing flash...")
                nvm.erase_flash(0x2000, 0x2000+size)
                time.sleep(1.5)
                # input("press key")
                # Program flash
                print("Programming flash...")
                nvm.program_flash(0x2000, memblock)
                end = time.time()
                time.sleep(0.5)
                _samba_op.uart.reset_mcu()
                print("Elapsed time %.6f" % (end-start))

            else:
                print("ERROR - Could not load bitfile {}, check filepath".format(bitfile))
                return False
        else:
            print("ERROR - No CPLD bitfile specified")
            return False
        return True
