import os
import sys
from pathlib import Path
from setuptools import setup, find_packages

def find_all_files(directory, endings=None, names=None):
    """
    Recursively find all files within directory
    """
    result = []
    for root, _, filenames in os.walk(directory):
        for filename in filenames:
            ending = os.path.splitext(filename)[-1]
            if endings is None and names is None:
                result.append(str(Path(root) / filename))
                print("Adding : ",str(Path(root) / filename))
            elif endings is not None and ending in endings:
                result.append(str(Path(root) / filename))
                print("Adding by ending : ",str(Path(root) / filename))
            if names is not None:
                str_filename = os.path.basename(filename)
                if filename in names:
                    result.append(str(Path(root) / filename))
                    print("Adding by name: ",str(Path(root) / filename))
    return result

DATA_FILES = []
DATA_FILES += find_all_files(str(Path("itpm_bios_v1_6")), endings=[".bit"])
DATA_FILES += find_all_files(str(Path("itpm_bios_v1_6")), endings=[".bin"])
DATA_FILES += find_all_files(str(Path("itpm_bios_v1_6")), endings=[".md"])
DATA_FILES += find_all_files(str(Path("itpm_bios_v1_6")), names=["LICENSE"])
DATA_FILES = [os.path.relpath(file_name, "itpm_bios_v1_6") for file_name in DATA_FILES]

setup(
    name='itpm_bios',
    version='0.5.0',
    packages=['itpm_bios_v1_6'],
    package_data={'itpm_bios_v1_6': DATA_FILES},
    zip_safe=False,
    url='https://gitlab.com/sanitaseg/itpm-bios.git',
    license='',
    author='Sanitas EG',
    author_email='info@sanitaseg.it',
    description='BIOS for iTPM board',
    dependency_links=[],
    install_requires=["optparse-pretty","tabulate","console_progressbar"],
)
